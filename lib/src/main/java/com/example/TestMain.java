package com.example;

import org.apache.commons.codec.binary.Base64;

public class TestMain {
	public static void main(String[] args) {
		// encryption
		String input = args[0];
		byte[] byteArray = Base64.encodeBase64(input.getBytes());
		String encStr = new String(byteArray);
		System.out.println("Encoded Data: " + encStr);

		// decryption
		byte[] decodeBase64 = Base64.decodeBase64(encStr.getBytes());
		String normal = new String(decodeBase64);
		System.out.println("Decoded data: " + normal);
	}

}
